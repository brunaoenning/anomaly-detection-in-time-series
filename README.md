# Anomaly Detection In Time Series

## Objectives: Understand the given data and do a benchmark of different anomaly detection methods for pump malfunctioning.

## Methodology:
- Choose anomaly detection techniques for each anomaly type;
- Avoid Neural Networks;
- For each technique state why it was chosen and explain the functioning of the method;
- Search for practical implementation on the articles for each method;
- Point the necessary data preparation techniques in order to run each model;
- Apply the chosen models;
- Software: Github, Python, Jupyter Notebook and Data.

## Context Water Pump Case (Kaggle):
Pump unit is exposed to typical bearing failures, water/oil leaks and
electrical faults. Reactive maintenance results in higher downtime, higher repair
cost and high safety risks.
Predictive maintenance helps to estimate when maintenance should be performed
and helps avoiding break down, repair cost, safety risks and unnecessary regular
maintenance.

**Reference: SUNILKUMAR. Predictive Maintenance of Pumps: Leveraging Machine Learning for Maintenance. Medium, 11 out. 2021.**

## Authors: 
- Bruna Amador, brunaamador@alunos.utfpr.edu.br, UTFPR
- Côme Rodriguez, come.rodrig@gmail.com, UTC
- Jiaqi Liu, corrine.liujq@gmail.com, UTC
- Marcelo Guimarães, marcelocosta@alunos.utfpr.edu.br, UTFPR
